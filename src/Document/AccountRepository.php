<?php

namespace App\Document;

use \Doctrine\ODM\MongoDB\DocumentManager;

use \App\Document\AccountRepositoryInterface;


class AccountRepository implements AccountRepositoryInterface
{

    private $md;

    public function __construct(DocumentManager $md)
    {
        $this->md = $md;

    }

    public function findAccountAll(): ?array
    {

        return $this->md->createAggregationBuilder(\App\Document\Account::class)
            ->lookup('Metric')
            ->localField('accountId')
            ->foreignField('accountId')
            ->alias('metrics')
            ->match()
            ->field('status')
            ->equals('ACTIVE')
            ->addFields()
            ->field('sumSpend')
            ->expression(['$sum' => '$metrics.spend'])
            ->addFields()
            ->field('sumImpressions')
            ->expression(['$sum' => '$metrics.impressions'])
            ->addFields()
            ->field('sumClicks')
            ->expression(['$sum' => '$metrics.clicks'])
            ->addFields()
            ->field('totalCostPerClick')
            ->expression(['$cond' => [['$eq' => ['$sumClicks', 0]], 0, ['$divide' => ['$sumSpend', '$sumClicks']]]])
            ->project()
            ->excludeFields(['_id'])
            ->includeFields(['accountName', 'accountId',
                'sumSpend', 'sumImpressions', 'sumClicks', 'totalCostPerClick'])
            ->getAggregation()->getIterator()->toArray();

    }

    public function findAccountByIdAccount(string $accountId): ?array
    {

        return $this->md->createAggregationBuilder(\App\Document\Account::class)
            ->lookup('Metric')
            ->localField('accountId')
            ->foreignField('accountId')
            ->alias('metrics')
            ->match()
            ->field('status')
            ->equals('ACTIVE')
            ->match()
            ->field('accountId')
            ->equals($accountId)
            ->addFields()
            ->field('sumSpend')
            ->expression(['$sum' => '$metrics.spend'])
            ->addFields()
            ->field('sumImpressions')
            ->expression(['$sum' => '$metrics.impressions'])
            ->addFields()
            ->field('sumClicks')
            ->expression(['$sum' => '$metrics.clicks'])
            ->addFields()
            ->field('totalCostPerClick')
            ->expression(['$cond' => [['$eq' => ['$sumClicks', 0]], 0, ['$divide' => ['$sumSpend', '$sumClicks']]]])
            ->project()
            ->excludeFields(['_id'])
            ->includeFields(['accountName', 'accountId',
                'sumSpend', 'sumImpressions', 'sumClicks', 'totalCostPerClick'])
            ->getAggregation()->getIterator()->toArray();

    }
}