<?php

namespace App\Document;


interface AccountRepositoryInterface
{

    public function findAccountAll(): ?array;

    public function findAccountByIdAccount(string $accountId): ?array;

}