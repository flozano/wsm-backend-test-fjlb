<?php

namespace App\Controller;


use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Document\AccountRepository;

class ReportController extends AbstractController
{

    /**
     * @Route("/", name="report", methods={"POST", "GET"})
     */
    public function index(Request $request, DocumentManager $md): Response
    {

        $accountId = $request->get('account_id');
        $accountsRepository = new AccountRepository($md);

        if ($accountId) {

           $accounts = $accountsRepository->findAccountByIdAccount($accountId);

        } else {

            $accounts = $accountsRepository->findAccountAll();

        }

        return $this->render('report.html.twig', [
            'message' => 'Insert Account ID',
            'accounts' => $accounts
        ]);

    }

}
